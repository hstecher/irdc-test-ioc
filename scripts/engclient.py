#!/usr/bin/env python3

"""GNIRS Engineering Script.

Usage:
  engclient.py bias <level>
  engclient.py setExposure <exptime> <fowler> <adc> <count>
  engclient.py expose 
  engclient.py sequence
  engclient.py interlockOn
  engclient.py interlockOff
  engclient.py rawOn
  engclient.py rawOff
  engclient.py aladdin2
  engclient.py aladdin3 
  engclient.py abort
  engclient.py init 
  engclient.py setClockouts <clockouts> 
  engclient.py resetArray 
  engclient.py readoutArray 
  engclient.py resetReadoutArray 
  engclient.py (-h | --help)

Options:
  level 	  shallow, medium, deep
  -h --help       This help message.

Notes:


"""

from threading import Event
from collections import namedtuple
import sys
import time

from docopt import docopt
import epics
from epics import caput, caget, cainfo
from epics import PV

PREFIX='irdc'

def bias(level):
   print('\nSetting bias level')
   if level == 'shallow':
      caput('irdc:biasLow.PROC', '1', wait=True)
      print('shallow -3.6V')
   elif level == 'medium':
      caput('irdc:biasMed.PROC', '1', wait=True)
      print('medium -3.4V') 
   elif level == 'deep':
      caput('irdc:biasHigh.PROC', '1', wait=True)
      print('deep -3.2V')


def setExposure(exposureTime, fowlerSamples, adcSamples, count=1):

   print('\nSending expose settings...')
   print('Exposure time: ' + str(exposureTime) + ' Fowler Samples: ' + str(fowlerSamples) + ' ADC Samples: ' + str(adcSamples))
   caput('irdc:setExposure.A', str(fowlerSamples)) 
   caput('irdc:setExposure.B', str(adcSamples)) 
   caput('irdc:setExposure.C', str(exposureTime)) 
   caput('irdc:setExposure.F', str(count)) 


def expose():

   caput('irdc:expose.INPA', b'1', wait=True)

   print('Sending expose command...')
   caput('irdc:expose.PROC', '1', wait=True)
   print('Request complete...see log for details\n')

   print('Exposure complete')

def sequence():
    print('Starting sequence of ' + str(count))
    expose()

    print('Sequence request complete...see log\n')


def init():
    print('\nInitializing controller')
    caput('irdc:init.PROC', '1', wait=True)

    print('Initialization complete\n')


def setClockouts(clockouts):
    print('\nSending Clockouts num: ' + str(clockouts))
    caput('irdc:clockoutNum.A', str(clockouts), wait=True)

    print('Command sent\n')


def resetArray():
    print('\nSending resetArray CA command')
    caput('irdc:resetArray.PROC', '1', wait=True)

    print('Command sent\n')

def readoutArray():
    print('\nSending readoutArray CA command')
    caput('irdc:readoutArray.PROC', '1', wait=True)

    print('Command sent\n')

def resetReadoutArray():
    print('\nSending resetReadoutArray CA command')
    caput('irdc:resetReadArray.PROC', '1', wait=True)

    print('Command sent\n')

def abort():
    print('\nAborting exposure')
    caput('irdc:abortExposure.PROC', '1', wait=True)

    print('Abort complete\n')

def interlock(activate):
    deactivate = 1

    if activate:
       print('\nTemerature interlock activated\n')
       deactivate = 0
    else:
       print('\nWARNING: Temperature interlock deactivated\n')
    	
    caput('irdc:setExposure.D', str(deactivate), wait=True)

def raw(include_raw):
    raw_int = 0;

    if include_raw:
       print('\nIncluding raw data in Fits\n')
       raw_int = 1
    else:
       print('\nNot including raw data in Fits\n')
    	
    caput('irdc:setExposure.G', str(raw_int), wait=True)




def aladdinIII(activate):
    if activate:
       print('\nAladdin III firmware enabled\n')
       caput('irdc:setExposure.E', '1', wait=True) 
       init()
    else:
       print('\nAladdin II firmware enabled\n')
       caput('irdc:setExposure.E', '0', wait=True)
       init()

def main():
    args = docopt(__doc__, version="GNIRS Engineering Script v0.1")

    if args['init']:
        init()
    elif args['bias']:
        bias(args['<level>'])
    elif args['interlockOn']:
        interlock(True)
    elif args['interlockOff']:
        interlock(False)
    elif args['rawOn']:
        raw(True)
    elif args['rawOff']:
        raw(False)
    elif args['aladdin3']:
        aladdinIII(True)
    elif args['aladdin2']:
        aladdinIII(False)
    elif args['setExposure']:
        sys.exit(setExposure(float(args['<exptime>']), int(args['<fowler>']), int(args['<adc>']), int(args['<count>'])))
    elif args['expose']:
        sys.exit(expose())
    elif args['sequence']:
        sys.exit(sequence())
    elif args['abort']:
        abort()       
    elif args['setClockouts']:
        setClockouts(int(args['<clockouts>']))       
    elif args['resetArray']:
        resetArray()       
    elif args['readoutArray']:
        readoutArray()       
    elif args['resetReadoutArray']:
        resetReadoutArray()       

if __name__ == '__main__':
    main()
