#!../../bin/linux-x86_64/gnirsDC

#- You may have to change gnirsDC to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/gnirsDC.dbd"
gnirsDC_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadTemplate "db/user.substitutions"
#dbLoadRecords "db/gnirsDCVersion.db", "user=root"
dbLoadRecords "db/dbSubExample.db", "user=irdc"

#- Set this to see messages from mySub
#var mySubDebug 1

#- Run this to trace the stages of iocInit
#traceIocInit

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncExample, "user=root"
