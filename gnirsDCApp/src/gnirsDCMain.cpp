/* gnirsDCMain.cpp */
/* Author:  Marty Kraimer Date:    17MAR2000 */

#include <stddef.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <unistd.h>


#include "epicsExit.h"
#include "epicsThread.h"
#include "iocsh.h"
#include "subRecord.h"
#include "registryFunction.h"
#include "epicsExport.h"

#include <libgnirsioc.h>
#include "cadef.h"

controllerInterface controller;

double pvGetDouble(std::string pvName);

class temperatureInterlock {
public:
	temperatureInterlock(double, double, std::string);
	bool isLocked();
	double getTemperature();

private:
	double tempLow;
	double tempHigh;
	std::string pvName;
};



temperatureInterlock::temperatureInterlock(double low, double high, std::string pv) : tempLow{low}, tempHigh{high}, pvName{pv} {};

bool temperatureInterlock::isLocked() {
	printf("Checking temperature...pvget\n");

	double temp = pvGetDouble(pvName.c_str());
	bool locked = true;

	if (temp > tempLow && temp < tempHigh)
		locked = false;

	return locked; 
}

double temperatureInterlock::getTemperature() {
	return pvGetDouble(pvName.c_str());
}

const int lowTemp = 0;
const int highTemp = 38; //Kelvin
temperatureInterlock lakeshoreIN1(lowTemp, highTemp, "tgnirs:ls325:IN1");
temperatureInterlock lakeshoreIN2(lowTemp, highTemp, "tgnirs:ls325:IN2");

int gIsInterlockOverride = 0;
bool gIncludeRaw = false;

void interlockCheck(subRecord *prec, void (*funct)()) {

	printf("Interlock check\n");

	if (!gIsInterlockOverride && lakeshoreIN1.isLocked()) {
		printf("Interlock: Temperature not in range. Temp: %f\n", lakeshoreIN1.getTemperature());
	}
	else {
		funct();
	}

}


void interlockAndMutexCheck(subRecord *prec, void (*funct)()) {

	printf("Interlock and mutex check\n");

	prec->val = 1;

	if (!gIsInterlockOverride && lakeshoreIN1.isLocked()) {
		printf("Interlock: Temperature not in range. Temp: %f\n", lakeshoreIN1.getTemperature());
	}
	else {
		controller.busyMutex.lock();
		funct();
		controller.busyMutex.unlock();
	}

	prec->val = 10;
}




bool gInitialized = false;
static long init(subRecord *prec) {

	interlockAndMutexCheck(prec, []() {

		printf("Initiazing Controller\n");	
		controller.init();
		gInitialized = true;

	});
		
	return 0;
}

static long biasLow(subRecord *prec) {

	interlockAndMutexCheck(prec, []() {

		printf("Set bias shallow\n");	
		controller.biasLow();

	});

	return 0;
}

static long biasMed(subRecord *prec) {
	interlockAndMutexCheck(prec, []() {

		printf("Set bias medium\n");	
		controller.biasMed();

	});

	return 0;
}

static long biasHigh(subRecord *prec) {
	interlockAndMutexCheck(prec, []() {

		printf("Set bias deep\n");	
		controller.biasHigh();

	});

	return 0;
}

static long clockoutNum(subRecord *prec) {
//	interlockAndMutexCheck(prec, []() {

		printf("Set clockout number: %i\n", (int)prec->a);	
		controller.setNumClockouts((int)prec->a);

//	});

	return 0;
}

static long abortExposure(subRecord *prec) {
	interlockCheck(prec, []() {

		printf("aborting exposure\n");	
		controller.abortExposure();
		printf("aborting exposure complete\n");	

	});

	return 0;
}

static long resetArray(subRecord *prec) {
	interlockAndMutexCheck(prec, []() {

		printf("Reset array\n");	
		controller.resetArray();

	});

	return 0;
}
static long resetReadArray(subRecord *prec) {
	interlockAndMutexCheck(prec, []() {

		printf("Reset Read Array\n");	
		controller.resetReadArray();

	});

	return 0;
}
static long readoutArray(subRecord *prec) {
	interlockAndMutexCheck(prec, []() {

		printf("Readout Array\n");	
		controller.readoutArray();

	});

	return 0;
}

static long expose(subRecord *prec) {
	interlockCheck(prec, []() {

		printf("Exposing\n");	

		if (gInitialized) {
			
			controller.startExposure(lakeshoreIN1.getTemperature(), lakeshoreIN2.getTemperature(), gIncludeRaw);
		}
		else {
			printf("Controller not initialized\n");
		}

	});

	return 0;
}

static long setExposure(subRecord *prec) {

//        interlockAndMutexCheck(prec, [prec]() {

		printf("Updating settings\n");

		controller.setExposure(prec->a, prec->b, prec->c, prec->f);

		gIsInterlockOverride = (int)prec->d;

		controller.setAladdinIII((int)prec->e == 1);

		gIncludeRaw = (int)prec->g == 1;

 //       });

        return 0;
}

double pvGetDouble(std::string pvName) {
	double	data = -1;
	chid	mychid;

	SEVCHK(ca_context_create(ca_disable_preemptive_callback),"ca_context_create");
	SEVCHK(ca_create_channel(pvName.c_str(),NULL,NULL,10,&mychid),"ca_create_channel failure");
	SEVCHK(ca_pend_io(5.0),"ca_pend_io failure");
	SEVCHK(ca_get(DBR_DOUBLE,mychid,(void *)&data),"ca_get failure");
	SEVCHK(ca_pend_io(5.0),"ca_pend_io failure");

	printf("%s %f\n",pvName.c_str(),data);

	return data;
}

void handler(int sig) {

        void *array[10];
        size_t size;

        //get void*'s for all entries on the stack
        size = backtrace(array, 10);
        
        // print out all the frames to stderr
        fprintf(stderr, "Error: signal %d:\n", sig);
        fprintf(stderr, "Stack trace: \n");
        backtrace_symbols_fd(array, size, STDERR_FILENO);
        exit(1);
      
}
        
        
int main(int argc,char *argv[])
{

/*
    if (signal(SIGSEGV, handler) == SIG_ERR)
        printf("Error setting up signal handler\n");

    if (signal(SIGKILL, handler) == SIG_ERR)
        printf("Error setting up signal handler\n");
*/


    if(argc>=2) {
        iocsh(argv[1]);
        epicsThreadSleep(.2);
    }
    iocsh(NULL);
    epicsExit(0);
    return(0);
}

epicsRegisterFunction(init);
epicsRegisterFunction(biasLow);
epicsRegisterFunction(biasMed);
epicsRegisterFunction(biasHigh);
epicsRegisterFunction(expose);
epicsRegisterFunction(setExposure);
epicsRegisterFunction(abortExposure);
epicsRegisterFunction(resetArray);
epicsRegisterFunction(resetReadArray);
epicsRegisterFunction(readoutArray);
epicsRegisterFunction(clockoutNum);

